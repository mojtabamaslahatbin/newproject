const phonesContainer = document.querySelector("#availableProduct");
const cart = document.querySelector('#dropArea');


// AJAX Call
const xhr = new XMLHttpRequest();
xhr.open('GET', 'StockData.json', true);

xhr.onload = () => {
    if (xhr.readyState === 4 && xhr.status === 200) {
        const elements = JSON.parse(xhr.responseText);
        renderHTML(elements);
    } else {
        console.log("can't reach database")
    }
};
xhr.send();

// Create DOM Elements
function renderHTML(elements) {
    elements.forEach((elementsItem) => {
        const htmlString = `<div class="phone card mb-3" draggable="true" id="drag">
                                <div class="row no-gutters ">
                                    <div class="col-md-4">
                                        <img src="pics/${elementsItem.model}.jpg" class="card-img" alt="...">
                                    </div>
                                    <div class="col-md-8">
                                        <div class="card-body">
                                            <h5 class="card-title">${elementsItem.model}</h5>
                                            <p class="card-text">Price :${elementsItem.price}$</p>
                                            <p class="card-text"><small class="text-muted">in stock : ${elementsItem.inStock} </small></p>
                                        </div>
                                    </div>
                                </div>
                           </div>`;
        phonesContainer.insertAdjacentHTML("afterbegin", htmlString);
    });

    // Drag and Drop elements
    const phones = document.querySelectorAll('.phone');
    let draggedItem = null;
    phones.forEach((phonesItem) => {

        phonesItem.addEventListener('dragstart', () => {
            draggedItem = phonesItem;
            setTimeout(() => { phonesItem.classList.add("invisible"); }, 0);

            cart.addEventListener('dragover', (e) => {
                e.preventDefault();
            });
            cart.addEventListener('dragenter', (e) => {
                e.preventDefault();
                e.target.classList.add("highlight-bgcolor");
            });
            cart.addEventListener('dragleave', (e) => {
                e.preventDefault();
                e.target.classList.remove("highlight-bgcolor");
            });
            cart.addEventListener('drop', (e) => {
                cart.appendChild(draggedItem);
                e.target.classList.remove("highlight-bgcolor");

            });
        });
        phonesItem.addEventListener('dragend', () => {
            phonesItem.classList.remove("invisible");
            draggedItem = null;
        });
    });
}